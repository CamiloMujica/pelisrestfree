package root.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-12-06T03:07:16")
@StaticMetamodel(PelisRestFree.class)
public class PelisRestFree_ { 

    public static volatile SingularAttribute<PelisRestFree, String> peli_cast;
    public static volatile SingularAttribute<PelisRestFree, Integer> peli_year;
    public static volatile SingularAttribute<PelisRestFree, String> peli_review;
    public static volatile SingularAttribute<PelisRestFree, String> peli_director;
    public static volatile SingularAttribute<PelisRestFree, String> peli_name;
    public static volatile SingularAttribute<PelisRestFree, Integer> id;
    public static volatile SingularAttribute<PelisRestFree, String> peli_category;

}