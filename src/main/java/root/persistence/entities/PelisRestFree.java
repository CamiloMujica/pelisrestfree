package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "pelisrestfree")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PelisRestFree.findAll", query = "SELECT p FROM PelisRestFree p"),
    @NamedQuery(name = "PelisRestFree.findById", query = "SELECT p FROM PelisRestFree p WHERE p.id = :id"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Name", query = "SELECT p FROM PelisRestFree p WHERE p.peli_name = :peli_name"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Year", query = "SELECT p FROM PelisRestFree p WHERE p.peli_year = :peli_year"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Review", query = "SELECT p FROM PelisRestFree p WHERE p.peli_review = :peli_review"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Cast", query = "SELECT p FROM PelisRestFree p WHERE p.peli_cast = :peli_cast"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Category", query = "SELECT p FROM PelisRestFree p WHERE p.peli_category = :peli_category"),
    @NamedQuery(name = "PelisRestFree.findByPeli_Director", query = "SELECT p FROM PelisRestFree p WHERE p.peli_director = :peli_director")})
public class PelisRestFree implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "peli_name")
    private String peli_name;
    @Basic(optional = false)
    @Column(name = "peli_year")
    private int peli_year;
    @Basic(optional = false)
    @Column(name = "peli_review")
    private String peli_review;
    @Basic(optional = false)
    @Column(name = "peli_cast")
    private String peli_cast;
    @Basic(optional = false)
    @Column(name = "peli_category")
    private String peli_category;
    @Basic(optional = false)
    @Column(name = "peli_director")
    private String peli_director;

    public PelisRestFree() {
    }

    public PelisRestFree(Integer id) {
        this.id = id;
    }

    public PelisRestFree(Integer id, String peli_name, int peli_year, String peli_review, String peli_cast, String peli_category, String peli_director) {
        this.id = id;
        this.peli_name = peli_name;
        this.peli_year = peli_year;
        this.peli_review = peli_review;
        this.peli_cast = peli_cast;
        this.peli_category = peli_category;
        this.peli_director = peli_director;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeli_name() {
        return peli_name;
    }

    public void setPeli_name(String peli_name) {
        this.peli_name = peli_name;
    }

    public int getPeli_year() {
        return peli_year;
    }

    public void setPeli_year(int peli_year) {
        this.peli_year = peli_year;
    }

    public String getPeli_review() {
        return peli_review;
    }

    public void setPeli_review(String peli_review) {
        this.peli_review = peli_review;
    }

    public String getPeli_cast() {
        return peli_cast;
    }

    public void setPeli_cast(String peli_cast) {
        this.peli_cast = peli_cast;
    }

    public String getPeli_category() {
        return peli_category;
    }

    public void setPeli_category(String peli_category) {
        this.peli_category = peli_category;
    }

    public String getPeli_director() {
        return peli_director;
    }

    public void setPeli_director(String peli_director) {
        this.peli_director = peli_director;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PelisRestFree)) {
            return false;
        }
        PelisRestFree other = (PelisRestFree) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.pelisrestfree.PelisRestFree[ id=" + id + " ]";
    }
    
}
